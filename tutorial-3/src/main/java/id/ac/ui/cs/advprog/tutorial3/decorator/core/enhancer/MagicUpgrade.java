package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random rng = new Random();
        return weapon.getWeaponValue() + 15 + rng.nextInt(6);
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getName() + " is now more magical!";
    }
}
