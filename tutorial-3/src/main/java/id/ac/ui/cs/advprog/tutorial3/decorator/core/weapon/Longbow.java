package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
    public Longbow(){
        weaponName = "Longbow";
        weaponDescription = "Bow that is long.";
        weaponValue = 15;
    }
}
