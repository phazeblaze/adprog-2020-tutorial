package id.ac.ui.cs.advprog.tutorial3.decorator.controller;

import id.ac.ui.cs.advprog.tutorial3.decorator.service.EnhanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class EnhanceController {

    @Autowired
    EnhanceService enhanceService;

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("weapons", enhanceService.getAllWeapons());
        return "decorator/home";
    }

    @RequestMapping(path = "/enhance", method = RequestMethod.POST)
    public String enhance(){
        enhanceService.enhanceAllWeapons();
        return "redirect:/home";
    }

}
