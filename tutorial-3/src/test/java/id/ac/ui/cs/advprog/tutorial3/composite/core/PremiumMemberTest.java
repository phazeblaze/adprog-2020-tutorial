package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.graalvm.compiler.core.common.type.ArithmeticOpTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member testChild = new OrdinaryMember("Peon", "Ordinary");
        member.addChildMember(testChild);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member testChild = new OrdinaryMember("Peon", "Ordinary");
        member.addChildMember(testChild);
        member.removeChildMember(testChild);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Peon", "Ordinary"));
        member.addChildMember(new OrdinaryMember("Peon", "Ordinary"));
        member.addChildMember(new OrdinaryMember("Peon", "Ordinary"));
        member.addChildMember(new OrdinaryMember("Peon", "Ordinary"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Big Boss", "Master");
        guildMaster.addChildMember(new OrdinaryMember("Peon1", "Ordinary"));
        guildMaster.addChildMember(new OrdinaryMember("Peon2", "Ordinary"));
        guildMaster.addChildMember(new OrdinaryMember("Peon3", "Ordinary"));
        guildMaster.addChildMember(new OrdinaryMember("Peon4", "Ordinary"));
        assertEquals(4, guildMaster.getChildMembers().size());
    }
}
