package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
        public String defend(){
            return "try getting past this";
        }
        public String getType(){
            return "this a barrier";
        }
}
