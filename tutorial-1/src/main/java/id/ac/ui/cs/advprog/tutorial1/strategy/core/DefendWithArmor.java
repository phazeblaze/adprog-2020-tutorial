package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String defend(){
        return "ENDURE";
    }
    public String getType(){
        return "AAAAAAARmore";
    }
}
