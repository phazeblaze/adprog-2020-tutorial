package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                guild.add(this);
        }

        public void update(){
                Quest quest = this.guild.getQuest();
                String type = this.guild.getQuestType();
                if (type.equals("R") || type.equals("D")){
                        this.getQuests().add(quest);
                }

        }
}
