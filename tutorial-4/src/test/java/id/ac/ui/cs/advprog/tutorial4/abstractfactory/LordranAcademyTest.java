package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof  MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Lordran Majestic");
        assertEquals(metalClusterKnight.getName(), "Lordran Metal Cluster");
        assertEquals(syntheticKnight.getName(), "Lordran Synthetic");

        assertEquals(majesticKnight.getArmor().getName(), "Shining Armor");
        assertEquals(majesticKnight.getWeapon().getName(), "Shining Buster");

        assertEquals(metalClusterKnight.getArmor().getName(), "Shining Armor");
        assertEquals(metalClusterKnight.getSkill().getName(), "Shining Force");

        assertEquals(syntheticKnight.getSkill().getName(), "Shining Force");
        assertEquals(syntheticKnight.getWeapon().getName(), "Shining Buster");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(majesticKnight.getArmor().getDescription(), "A shiny armor.");
        assertEquals(majesticKnight.getWeapon().getDescription(), "A shining buster sword.");

        assertEquals(metalClusterKnight.getArmor().getDescription(), "A shiny armor.");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "It's shining!");

        assertEquals(syntheticKnight.getSkill().getDescription(), "It's shining!");
        assertEquals(syntheticKnight.getWeapon().getDescription(), "A shining buster sword.");
    }
}
