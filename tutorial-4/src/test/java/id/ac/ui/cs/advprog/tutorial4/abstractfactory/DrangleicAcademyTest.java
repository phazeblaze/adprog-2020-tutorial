package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");

    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof  MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Drangleic Majestic");
        assertEquals(metalClusterKnight.getName(), "Drangleic Metal Cluster");
        assertEquals(syntheticKnight.getName(), "Drangleic Synthetic");

        assertEquals(majesticKnight.getArmor().getName(), "Metal Armor");
        assertEquals(majesticKnight.getWeapon().getName(), "Thousand Jacker");

        assertEquals(metalClusterKnight.getArmor().getName(), "Metal Armor");
        assertEquals(metalClusterKnight.getSkill().getName(), "Thousand Years of Pain");

        assertEquals(syntheticKnight.getWeapon().getName(), "Thousand Jacker");
        assertEquals(syntheticKnight.getSkill().getName(), "Thousand Years of Pain");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(majesticKnight.getArmor().getDescription(), "Armor made of metal.");
        assertEquals(majesticKnight.getWeapon().getDescription(), "Yikes");

        assertEquals(metalClusterKnight.getArmor().getDescription(),"Armor made of metal.");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "Ouch!");

        assertEquals(syntheticKnight.getWeapon().getDescription(), "Yikes");
        assertEquals(syntheticKnight.getSkill().getDescription(), "Ouch!");
    }

}
