package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @Test
    public void HolyWishTest(){
        HolyGrail grail = new HolyGrail();
        HolyWish grailWish = grail.getHolyWish();
        HolyWish wish = HolyWish.getInstance();

        assertEquals(grailWish, wish);
    }
    @Test
    public void MakeAWishTest(){
        HolyGrail grail = new HolyGrail();
        grail.makeAWish("Pengen dapet orundum");
        assertEquals(grail.getHolyWish().getWish(), "Pengen dapet orundum");
    }
}
