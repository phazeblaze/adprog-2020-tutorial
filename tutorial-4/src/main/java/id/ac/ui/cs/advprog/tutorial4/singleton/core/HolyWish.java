package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach
    private static HolyWish single_instance = null;

    private HolyWish(){
        this.wish = "I wish to restore Britain to its former glory.";
    }

    public static HolyWish getInstance(){
        if (single_instance == null){
            single_instance = new HolyWish();
        }

        return single_instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
