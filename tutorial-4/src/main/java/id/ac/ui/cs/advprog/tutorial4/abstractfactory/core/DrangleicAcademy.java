package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

public class DrangleicAcademy extends KnightAcademy {

    @Override
    public String getName() {
        // TODO complete me
        return "Drangleic";
    }

    @Override
    protected Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new DrangleicArmory();

        switch (type) {
            case "majestic":
                // TODO complete me
                knight = new MajesticKnight(armory);
                knight.setName("Drangleic Majestic");

                break;
            case "metal cluster":
                // TODO complete me
                knight = new MetalClusterKnight(armory);
                knight.setName("Drangleic Metal Cluster");

                break;
            case "synthetic":
                // TODO complete me
                knight = new SyntheticKnight(armory);
                knight.setName("Drangleic Synthetic");
                break;
        }

        return knight;
    }
}
