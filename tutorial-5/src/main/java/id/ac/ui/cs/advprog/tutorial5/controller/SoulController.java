package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Signature;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;

    public SoulController(SoulService soulService){
        this.soulService = soulService;
    }


    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        // TODO: Use service to complete me.
        return new ResponseEntity<List<Soul>>(soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        // TODO: Use service to complete me.
        soulService.register(soul);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // TODO: Use service to complete me.
        Optional<Soul> oSoul = soulService.findSoul(id);
        if (oSoul.isPresent()){
            return new ResponseEntity<Soul>(oSoul.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.
        Optional<Soul> oSoul = soulService.findSoul(id);
        if(oSoul.isPresent()){
            return new ResponseEntity<>(soulService.rewrite(soul), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        // TODO: Use service to complete me.
        Optional<Soul> oSoul = soulService.findSoul(id);
        if(oSoul.isPresent()){
            soulService.erase(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
