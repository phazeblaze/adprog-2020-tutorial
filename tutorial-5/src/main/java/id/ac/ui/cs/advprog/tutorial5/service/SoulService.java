package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import java.util.List;
import java.util.Optional;

public interface SoulService {
    public List<Soul> findAll();
    public Optional<Soul> findSoul(Long id);
    public void erase(Long id);
    public Soul rewrite(Soul soul);
    public Soul register(Soul soul);
}
