package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRepository repository;

    private Soul soul;

    @InjectMocks
    private SoulServiceImpl service;

    @BeforeEach
    public void setUp(){
        soul = new Soul( "Shadow", 19, "?", "Dummy");
    }

    @Test
    public void testFindAll(){
        List<Soul> soulList = service.findAll();
        lenient().when(service.findAll()).thenReturn(soulList);
    }

    @Test
    public void testRegisterSoul(){
        service.register(soul);
        lenient().when(service.register(soul)).thenReturn(soul);
    }

    @Test
    public void testFindSoul(){
        service.register(soul);
        Optional<Soul> optionalSoulExist = service.findSoul(soul.getId());
        Optional<Soul> optionalSoulDoesntExist = service.findSoul((long) 2);
        lenient().when(service.findSoul(soul.getId())).thenReturn(Optional.of(soul));
    }

    @Test
    public void testErase(){
        service.register(soul);
        service.erase(soul.getId());
        lenient().when(service.findSoul(soul.getId())).thenReturn(Optional.empty());
    }

    @Test
    public void testRewriteSoul(){
        service.register(soul);
        assertEquals(soul,service.rewrite(soul));
    }

}
