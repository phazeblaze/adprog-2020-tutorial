package id.ac.ui.cs.advprog.tutorial5.core;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest{
    Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul("name", 20, "male", "worker");
    }

    @Test
    public void GetterTest(){
        //assertEquals(soul.getId(), 1);
        assertEquals(soul.getName(), "name");
        assertEquals(soul.getAge(), 20);
        assertEquals(soul.getGender(), "male");
        assertEquals(soul.getOccupation(), "worker");
    }

    public void SetterTest(){
        soul.setName("shojo");
        soul.setAge(18);
        soul.setGender("female");
        soul.setOccupation("student");

        assertEquals(soul.getName(), "shojo");
        assertEquals(soul.getAge(), 18);
        assertEquals(soul.getGender(), "female");
        assertEquals(soul.getOccupation(), "student");
    }
}