package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spellList;
    public ChainSpell(ArrayList<Spell> spellList){
        this.spellList = spellList;
    }

    @Override
    public void cast(){
        for (Spell spell : this.spellList){
            spell.cast();
        }
    }

    @Override
    public void undo(){
        for (int i = this.spellList.size()-1;i>=0;i-- ){
            spellList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
