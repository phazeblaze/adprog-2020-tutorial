package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import org.springframework.stereotype.Repository;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;

import java.util.*;

@Repository
public class ContractSeal {
    Spell latestSpell;

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell spell = spells.get(spellName);
        spell.cast();
        this.latestSpell = spell;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if(latestSpell != null) {
            this.latestSpell.undo();
            this.latestSpell = null;
        }

    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
